
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace SignalR.Server
{
    public class GreeterHub : Hub<IGreeterHubClient>
    {
        public Task<string> SayHello(string name)
        {
            return Task.FromResult($"Hello {name}");
        }

        public async Task SubscribeOnStock(string stockName)
        {
            Console.WriteLine($"Subscribed {Context.ConnectionId} to {stockName}");
            await Groups.AddToGroupAsync(Context.ConnectionId, stockName);
        }
    }

    public interface IGreeterHubClient
    {
        Task SayHelloOnClient(string name);
        Task ReceiveUpdate(string stock, decimal value);
    }
}