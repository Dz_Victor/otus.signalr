﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;

var stockName = "MSFT";
if ((args?.Length > 0))
    stockName = args[0];

Guid id = Guid.NewGuid();
Console.WriteLine($"client: {id}");

var connection = new HubConnectionBuilder()
        .WithUrl("https://localhost:5001/hubs/greeter")
        .WithAutomaticReconnect()
        .Build();

await connection.StartAsync();

connection.Reconnected += async (s) =>
{
    connection.On("ReceiveUpdate", (string stockName, decimal stockValue) =>
    {
        Console.WriteLine($"[{stockName}]: Received update {stockValue}");
    });
    Console.WriteLine($"ReSubscribe {stockName}");
    await connection.InvokeAsync("SubscribeOnStock", stockName);
};

connection.On("ReceiveUpdate", (string stockName, decimal stockValue) =>
{
    Console.WriteLine($"[{stockName}]: Received update {stockValue}");
});
Console.WriteLine($"Subscribe {stockName}");

await connection.InvokeAsync("SubscribeOnStock", stockName);

Console.ReadLine();
await connection.StopAsync();