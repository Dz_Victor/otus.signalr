import { HttpTransportType, HubConnection, HubConnectionBuilder, LogLevel } from "@microsoft/signalr";
import { Button, Input, notification } from "antd";
import React, { useEffect, useState } from "react";

const StockItem = () => {
  const [connection, setConnection] = useState<null | HubConnection>(null);
  const [inputText, setInputText] = useState("");
  const [stockValue, setStockValue] = useState(0);

  useEffect(() => {

    console.log("start");

    const connect = new HubConnectionBuilder()
      .configureLogging(LogLevel.Debug)
      .withUrl("https://localhost:5001/hubs/greeter", {
        skipNegotiation: true,
        transport: HttpTransportType.WebSockets
      })
      .withAutomaticReconnect()
      .build();

    setConnection(connect);
  }, []);

  useEffect(() => {
    if (connection) {
      connection
        .start()
        .then(() => {
          console.log("connect to stock info");
          connection.on("ReceiveUpdate", handleReceiveMessage);
        })
        .catch((error) => console.log(error));
    }
  }, [connection]);

  const handleReceiveMessage = (stockName: string, value: number) => {
    console.log("stockName: " + stockName);
    console.log("value: " + value);

    setStockValue(value);
  }

  const sendMessage = async () => {
    if (connection)
      await connection.send("SubscribeOnStock", inputText);
  };

  return (
    <>
      <div style={{ display: "flex" }} >
        <Input
          value={inputText}
          onChange={(input) => {
            setInputText(input.target.value);
          }}
        />
        <Button onClick={sendMessage} type="primary">
          Subscribe
        </Button>
      </div>
      <div style={{ fontSize: "18px", fontWeight: "bold", display: "flex", margin: "5px 0px 20px 5px" }}>
        {stockValue}
      </div>
    </>
  );
};

export default StockItem;